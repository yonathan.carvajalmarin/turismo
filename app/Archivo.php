<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{

    public function registro(){
        return $this->belongsTo(Registro_multimedia::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
       'video', 'image',
   ];
}
