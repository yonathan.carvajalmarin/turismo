<?php

use App\Registro_multimedia;
use Illuminate\Database\Seeder;

class RegistrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cantidadRegistros=10;
        factory(Registro_multimedia::class, $cantidadRegistros)->create();
    }
}
