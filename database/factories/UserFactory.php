<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Archivo;
use App\Registro_multimedia;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'last_name' => $faker->word,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

// factory tabla registros
$factory->define(Registro_multimedia::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraph(1),
        'title' => $faker->word,
        'date' => $faker->date,
        'users_id'=>User::all()->random()->id,
    ];
});

// factory tabla archivos
$factory->define(Archivo::class, function (Faker $faker) {
    return [
        'video' => $faker->word,
        'image' => $faker->randomElement(['image1','image2','image3','image4','image5','image6',
        'image7','imag8','image9','image10',]),
        'registro_id' =>Registro_multimedia::all()->random()->id,
    ];
});